if (!this.message) {
    error('message', "is required");
} else if (this.message.length > 160) {
    error('message', "must be below 160 characters");
} else {
    var allMentions = (this.message.match(/#[a-z0-9]+/ig) || []).map(function(m) {
        //Drop the @
        return m.replace('#', '');
    });
    
    this.mentions = allMentions.map(function(u) {  return u;  });
   
}

