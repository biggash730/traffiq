var deployd = require('deployd');

var app = deployd({
  port: process.env.PORT || 3000,
  env: 'staging',
  db: {
    host: 'ds053139.mongolab.com',
    port: 53139,
    name: 'traffiq',
    credentials: {
      username: 'biggash730',
      password: 'db.pass'
    }
  }
});

var traffiq = app.createStore('traffiq');
traffiq.remove(function() {
  app.listen();
});

app.on('error', function(err) {
  console.error(err);
  process.nextTick(function() { // Give the server a chance to return an error
    process.exit();
  });
});